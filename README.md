My development stack
===================


There are two new docker tools that makes life easier:

 1. [boot2docker](https://docs.docker.com/installation/mac/)
 1. [fig](http://www.fig.sh/install.html)

boo2docker makes the Vagrantfile obsolete.


Introduction
------------

Pre-requisites:

 * VirtualBox (virtualbox.org)
 * Vagrant (vagrantup.com)


Contents:

 * Ubuntu
 * Docker
 * NodeJS managed by nvm
 * redis
 * supervisor
 * jacc - a simple docker addon

Everything is installed using `Vagrantfile`. This is simply a copy of this file 
https://github.com/dotcloud/docker/blob/master/Vagrantfile with some addtions in
the init script.


Getting started
---------------

Do these steps in the repo:

1. `vagrant up` - this downloads ubuntu and runs the installation scripts
1. `vagrant halt` followed by `vagrant up` - reboot seams to be necessary to make 
sure guest addtions work (at least on OSX)
1. `vagrant ssh`



Getting oriented:

1. Do `nvm ls` to see what node versions that are installed
1. `nvm use v0.XX.X` will choose a NodeJS version
1. This repo should be available here:  `cat /vagrant/README.md`

Setup jacc:

1. Follow the instuction in the README file: `cat /usr/lib/node_modules/jacc/README.md`


Setting up a production server
------------------------------

Amazon EC2 servers are used for production. Docker has guide for setting up a
EC2 server with docker: http://docs.docker.io/en/latest/installation/amazon/

All that is necessary is to add `#include https://get.docker.io` into User Data
and docker will be bootstrapped into the new server. Choose a server of size
t1.medium and a root partition of at least 100GB (building docker images
consumes quote a ot of disk).

This installs the same stuff as the `Vagrantfile`:

```
#
# Nifty tools
#

sudo apt-get update
sudo apt-get install -y git unzip s3cmd curl redis-server supervisor


#
# install and configure nodejs
#

# install nvm for vagrant user
su vagrant -c "cd $HOME && wget -qO- https://raw.github.com/creationix/nvm/master/install.sh | sh"

# update nodejs
su vagrant -c "cd $HOME && source $HOME/.nvm/nvm.sh && nvm install v0.11.2"
su vagrant -c "cd $HOME && source $HOME/.nvm/nvm.sh && nvm use v0.11.2"

# Install jacc
su vagrant -c "source $HOME/.nvm/nvm.sh && nvm use v0.11.2 && sudo npm install jacc -g"

# install nvm for root user
source $HOME/.nvm/nvm.sh; nvm use v0.11.2; n=$(which node);n=${n%/bin/node}; chmod -R 755 $n/bin/*; sudo cp -r $n/{bin,lib,share} /usr
```

Setup `jacc` in the same way as above.

